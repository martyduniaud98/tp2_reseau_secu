# TP2

TP de simulation de réseau d'entreprise.

![](/screens/reseau.png)

<br>

## Sommaire

- [Mise en place des VMs](#mise-en-place-des-vms)
- [Création d'une DB MySQL MariaDB](#création-dune-db-mysql-mariadb)
- [Configuration du serveur web](#configuration-du-serveur-web)
- [Configuration du Pfsense](#configuration-du-pfsense)
- [Mise en place de l'IDS SNORT](#mise-en-place-de-lids-snort)

<br>

## Mise en place des VMs

- Vm pfsense (192.168.1.1):

  - 1 carte réseau WAN
  - 1 carte réseau LAN
  - 1 carte réseau DMZ

  PfSense est utilisé comme pare-feu et routeur. La configuration de ses interfaces, WAN, LAN et DMZ, permet de segmenter le réseau en fonction des besoins de sécurité.

<br>

- Vm Admin Linux GUI (192.168.1.102):

  - 1 carte réseau LAN

  Cette machine virtuelle est configurée avec une interface graphique Linux pour faciliter l'administration. Elle est positionnée sur le réseau LAN, ce qui lui permet d'accéder aux ressources internes du réseau.

<br>

- Vm Client Windows (192.168.1.100):

  - 1 carte réseau LAN

  Cette machine Windows sert à simuler un utilisateur sur le réseau. L'unique interface réseau et l'autorisation du ping sur le pare-feu permettent des tests de connectivité basiques.

  - Autorisation des ping sur le firewall Windows:
    ![](/screens/firewall.png)
    ![](/screens/rules.png)

  <br>

- Vm Linux - serveur web (192.168.1.103):

  - 1 carte réseau LAN
  - 1 carte réseau DMZ

  Cette VM sert à héberger un serveur web. Elle a une interface sur le réseau LAN pour être accessible en interne et une interface DMZ pour isoler le serveur des ressources sensibles du réseau local.

  <br>

- Vm Linux - DB (192.168.1.101):

  - 1 carte réseau LAN

  Cette VM sert de de base de données MariaDB. Elle est placée sur le réseau LAN pour permettre une communication avec le serveur web.

  <br>

## Création d'une DB MySQL MariaDB

Utilisation de MariaDB pour créer une base de données.

- Installation de MariaDB

```bash
sudo apt install mariadb-server
sudo mysql_secure_installation
```

- Première connexion MariaDB

```bash
sudo mysql -u root -p
```

```mysql
CREATE database person;
CREATE USER 'serveur'@'192.168.1.103' IDENTIFIED BY 'mysecretpassword';
GRANT ALL PRIVILEGES ON person.* TO 'serveur'@'192.168.1.103' WITH GRANT OPTION;
FLUSH PRIVILEGES;
.exit
```

La création d'une base de données nommée 'person', d'un utilisateur 'serveur' avec un mot de passe, et l'attribution des privilèges nécessaires pour accéder à la base de données depuis le serveur web sont effectuées.

<br>

Autoriser l'accès à la base de données depuis le serveur web :

```bash
sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf
# modifier la ligne bind-address = 127.0.0.1 par bind-address = 0.0.0.0
```

La modification du fichier de configuration 50-server.cnf pour autoriser l'accès distant à la base de données depuis le serveur web

![](/screens/db.png)

<br>

## Configuration du serveur web

Utilisation de Flask pour créer un petit serveur web.

- Installation de Flask

```bash
sudo apt install pip
pip install flask
pip install flask-alchemy
pip install pymysql
```

Les commandes installent Flask et ses dépendances, notamment Flask-Alchemy et PyMySQL, nécessaires pour créer un serveur web avec une base de données MariaDB.

Création du script python puis lancement du serveur python.

![](/screens/web.png)

Ce script utilise les informations de la base de données MariaDB pour fournir du contenu dynamique.

<br>

## Configuration du Pfsense

Des règles de pare-feu sont configurées sur PfSense pour contrôler le trafic entrant et sortant des réseaux LAN et DMZ. Ces règles définissent quel type de trafic est autorisé ou bloqué, renforçant ainsi la sécurité du réseau.

Ajout des règles de firewall pour la LAN

![](/screens/lan.png)

Ajout des règles de firewall pour la DMZ

![](/screens/dmz.png)

La Vm Windows est configurée pour autoriser les ping.

![](/screens/ping.png)

La Vm Admin System est configurée pour pouvoir se connecter en ssh sur toutes les machines du réseau.

![](/screens/ssh.png)

<br>

## Mise en place de l'IDS SNORT

- Installation de SNORT sur PfSense via le packet manager.
- Activation des téléchargements de règles gratuites, en cochant la première case (Enable Snort VRT)

  - S'incrire sur le site www.snort.org pour obtenir une clé d'activation

  - **Aller dans Services** -> **Snort** -> **Global Settings** puis renseigner la clé d'activation dans la case Oinkcode et cochez les cases :

    - **Enable Snort GPLv2**
    - **Enable ET Open**
    - **Enable OpenAppID**

    ![](/screens/rules_snort.png)

- Mettre à jour manuellement les règles cochées précédemment via l'onglet **Updates**

- Configuration du réseau sur lequel snort va écouter le traffic :

  - Choisir le réseau LAN
  - Cocher la case **Send Alerts to System Log**

  ![](/screens/alerts.png)

- Activation des règles précédemment téléchargées :

  - Aller dans l'onglet **Snort Interfaces** -> **LAN Categories** et cocher la case **Use IPS Policy**

  ![](/screens/policy.png)

  - Retourner sur **Snort Interfaces** et cliquer sur START.

- Test de l'IDS avec NMAP sur le réseau LAN

  ![](/screens/nmap.png)

- Vérification des alertes sur pfsense :

  - Aller dans l'onglet **Alerts** et nous pouvons y voir notre Nmap détecté par Snort.

    ![](/screens/ids.png)
